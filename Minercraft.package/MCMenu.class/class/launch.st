accessing
launch
| space menu play quit|

space := BlSpace new.
menu := MCMenu new.
play:= (MCMenuItem withLabel: 'Play').
quit := (MCMenuItem withLabel: 'Quit').
menu addChild: play.
menu addChild: quit.
play relocate: 50@100.
quit relocate: 50@200.
space root addChild: menu.

space show.