drawing
drawOnSpartaCanvas: canvas
|font|
font := canvas font
named: 'Source Sans Pro';
size: 50;
build.

canvas fill paint: (Color r:0 g:0 b:1); path: self boundsInLocal ; draw.
canvas text baseline: 0@40; font: font; string: itemLabel; draw.